angular.module('jomi.popupService',[])
.factory('popupService',['$state','$q','$ionicPopup','$ionicModal','$rootScope',function($state, $q, $ionicPopup,$ionicModal,$rootScope){
		var factory = {};

		factory.alert = function alert(title,message){
			return $ionicPopup.alert({title:title,template:message});
		};
    
		factory.confirm = function(title,message,cancel,ok){
			q = $q.defer();
			 var confirmPopup = $ionicPopup.confirm({
		     title: title,
		     template: message,
		     cancelText: cancel,
		     okText: ok
		   });

		   confirmPopup.then(function(res) {
			    q.resolve(res);
			   });
			 return q.promise;
		}
		return factory;

}]);
