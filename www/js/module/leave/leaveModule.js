angular.module('jomi.leaveModule', [])

.controller('leaveController', function($scope, $ionicModal, $timeout, $state, $http, $ionicLoading, $rootScope,httpInvoke,popupService) {
  $scope.model = {};
  $scope.addLeave = function (model) {
    var data = {
      'username' : $rootScope.loginData.username,
      'idLeaveType' : model.type,
      'description' : model.description
    }
    httpInvoke.invoke('nonbillablemandays/addNonBillableMandays', data).then(_onSuccess, _onFail);
    function _onSuccess(response){
      console.log(JSON.stringify(response));
      $state.go("home");
    }
    function _onFail(response){
      console.log(JSON.stringify(response));
      popupService.alert(response.messageCode);
    }
  }
});
