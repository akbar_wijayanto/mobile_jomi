angular.module('jomi.projectModule', [])

.controller('projectController', function($scope, $ionicModal, $timeout, $state, $http, $ionicLoading, $rootScope,httpInvoke,popupService) {

  $scope.init = function () {
    data = {
      'username' : $rootScope.loginData.username
    }
    httpInvoke.invoke('project/getCustomerProjects', data).then(_onSuccess, _onFail);
    function _onSuccess(response){
      if(response.hasProject == false){
          $scope.showEmptyList = true;
      }else{
          $scope.showEmptyList = false;
          $scope.projectsData = response.coreProject;
          console.log($scope.projectsData);
      }
      
    }
    function _onFail(response){
      console.log(JSON.stringify(response));
      popupService.alert(response.messageCode);
    }
  }

});
