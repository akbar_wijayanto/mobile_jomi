angular.module('jomi.changePasswordModule', [])

.controller('changePasswordController', function($scope, $ionicModal, $timeout, $state, $http, $ionicLoading, $rootScope, httpInvoke, popupService) {
  $scope.model = {};
  $scope.changePass = function (model) {
    var data = {
      'username' : $rootScope.loginData.username,
      'oldPassword' : model.oldPassword,
      'newPassword' : model.newPassword
    }
    httpInvoke.invoke('setting/changePassword', data).then(_onSuccess, _onFail);
    function _onSuccess(response){
      console.log(JSON.stringify(response));
      popupService.alert("Change password has been successful");
      $state.go("home");
    }
    function _onFail(response){
      console.log(JSON.stringify(response));
      popupService.alert(response.messageCode);
    }
  }
});
