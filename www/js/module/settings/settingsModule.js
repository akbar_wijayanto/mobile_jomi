angular.module('jomi.settingsModule', [])

.controller('settingsController', function($scope, $ionicModal, $timeout, $state, $http, $ionicLoading, $rootScope, httpInvoke, popupService) {
  $scope.goChangePass = function (model) {
    $state.go('changePasswordForm');
  }
});
