angular.module('jomi.taskModule', [])

.controller('taskController', function($scope, $ionicModal, $timeout, $state, $http, $ionicLoading,$rootScope,httpInvoke,popupService) {

  $scope.init = function () {
    data = {
      'username' : $rootScope.loginData.username
    }
    httpInvoke.invoke('task/getCustomerTasks', data).then(_onSuccess, _onFail);
    function _onSuccess(response){
      console.log(JSON.stringify(response));
      if(response.hasTask == false){
        $scope.showList = true;
      }else{
        $scope.taskData = response.dtos;
        console.log($scope.taskData);
        $scope.showList = false;
      }
    }
    function _onFail(response){
      console.log(JSON.stringify(response));
      popupService.alert(response.messageCode);
    }
  }

  $scope.taskDetail = function(param){
    console.log(param);
    console.log($scope.taskData);
    $scope.detail = $scope.taskData[param];
    console.log("detail : "+JSON.stringify($scope.detail));
    $state.go("taskDetail",{parameter:$scope.detail});
  }
})
.controller('taskDetailController', function($scope, $state, $stateParams, $rootScope, httpInvoke, popupService) {
  console.log("stateParam : "+JSON.stringify($stateParams.parameter))
  $scope.detail = $stateParams.parameter;
  $scope.model = {
    'id' : $scope.detail.taskId
  };

  $scope.updateTask = function (model) {
    data = {
      'username'          : $rootScope.loginData.username,
      'taskAssignmentId'  : model.id,
      'status'            : model.status
    }
    httpInvoke.invoke('task/updateTask', data).then(_onSuccess, _onFail);
    function _onSuccess(response){
      console.log(JSON.stringify(response));
      popupService.alert(response.messageCode);
      $state.go('home');
    }
    function _onFail(response){
      console.log(JSON.stringify(response));
      popupService.alert(response.messageCode);
    }
  }
});
