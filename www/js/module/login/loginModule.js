angular.module('jomi.loginModule', [])

.controller('loginController', function($scope, $ionicModal, $timeout, $state, $rootScope, $ionicLoading, $http, popupService, httpInvoke) {

  $scope.user={
    "username":null,
    "password":null
  };

  $scope.doLogin = function (form,request) {
    if(validate(request)) {
      var data = {
        'username' : $scope.user.username,
        'password' : $scope.user.password
      }
      httpInvoke.invoke('login', data).then(_onSuccess, _onFail);
      function _onSuccess(response){
        console.log(JSON.stringify(response));
        $state.go("home");
        $scope.user.username = null; //clear textbox value
        $scope.user.password = null; //clear textbox value
      }
      function _onFail(response){
        console.log(JSON.stringify(response));
        popupService.alert(response.messageCode);
        $scope.user.username = null; //clear textbox value
        $scope.user.password = null; //clear textbox value
      }
    }
  }

  function validate(request){
    var username = request.username || "";
    var password = request.password || "";
    if(username.trim().length<=0 && password.length<=0 ){
      popupService.alert("Please insert username and password");
      return false;
    }
    return true;
  }

});
