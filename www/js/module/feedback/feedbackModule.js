angular.module('jomi.feedbackModule', [])

.controller('feedbackController', function($scope, $ionicModal, $timeout, $state, $http, $ionicLoading, $rootScope, httpInvoke, popupService) {
  $scope.model = {};
  $scope.addFeedback = function (model) {
    var data = {
      'username' : $rootScope.loginData.username,
      'description' : model.description
    }
    httpInvoke.invoke('feedback/add', data).then(_onSuccess, _onFail);
    function _onSuccess(response){
      console.log(JSON.stringify(response));
      popupService.alert("Thank you for your feedback.");
      $state.go("home");
    }
    function _onFail(response){
      console.log(JSON.stringify(response));
      popupService.alert(response.messageCode);
    }
  }
});
