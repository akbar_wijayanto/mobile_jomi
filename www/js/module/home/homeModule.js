angular.module('jomi.homeModule', [])

.controller('homeController', function($scope, $ionicModal, $timeout, $state, $http, $ionicLoading) {

  /* LOGOUT */
  $scope.logout = function () {
    $state.go('login');
  }

  /* ABOUT US */
  $scope.aboutUs = function () {
    $state.go('aboutUs');
  }

  /* MY PROJECT */
  $scope.myProjects = function () {
    $state.go('myProjectList');
  }

  /* MY TASK */
  $scope.myTask = function () {
    $state.go('myTaskList');
  }

  /* LEAVE FORM */
  $scope.leave = function () {
    $state.go('leaveForm');
  }

  /* FEEDBACK FORM */
  $scope.feedback = function () {
    $state.go('feedbackForm');
  }

  /* SETTINGS FORM */
  $scope.setting = function () {
    $state.go('settingList');
  }

});
