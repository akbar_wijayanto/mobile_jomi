angular.module('jomi.http.invoke',[])
.factory('httpInvoke',['$rootScope','$state','$http','$q','$ionicLoading','popupService',function($rootScope, $state, $http,$q,$ionicLoading,popupService){
	var factory = {};
	factory.invoke=function(method,data){
		$ionicLoading.show({
      template: 'Loading ...',
    });
    var defer = $q.defer();

    var invoker = null;
    invoker = $http({
      method 	: 'POST',
      url 	  : baseUrl+"/"+method,
      timeout	: timeout,
      data 	  : data
    });

    invoker.success(function(data,status,headers,config){
      var responseWrapper = data;
        if(data.errorCode==1000){
          $rootScope.loginData = responseWrapper;
          $ionicLoading.hide();
          defer.resolve(responseWrapper);
        } else {
          console.log(data.messageCode);
          $ionicLoading.hide();
          defer.reject(responseWrapper);
        }
    });

		 invoker.error(function(data,status,headers,config){
			 $ionicLoading.hide();
			 popupService.alert(JSON.stringify(data));
		 })
		return defer.promise;
	}

	factory.errorCodeControl = function(msg){

		 var code = factory.getErrorCode(msg);
		 console.log(code);

		 //translate
		 errorMsg = 'ERRORCODE.'+code;
		 var message = translator.translate(errorMsg);

		 //check code
		 if(code == 3033){
			 //get number
			 var number = msg.substr(15,12);
			 console.log(number);
			 message = number + ' : '+ message;
		 }

		 //check message if not found on lang.js
		 if(message == errorMsg){
			// if not found error code, use default message from response
			 msg = factory.getDefaultErrorCodeMsg(msg);
		 } else {
			 msg = message;
		 }
		 console.log(msg)
		return msg;

	};

	factory.getErrorCode = function(msg){
		var code = msg.substr(0,4);
		return code;
	};

	factory.getDefaultErrorCodeMsg = function(msg){
		var defaultMsg = msg.substring(7);
		return defaultMsg;
	}

	return factory;
}]);
