// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('jomi', ['ionic', 'jomi.controllers',

  'jomi.loginModule', 'jomi.homeModule', 'jomi.aboutUsModule', 'jomi.projectModule',
  'jomi.taskModule', 'jomi.leaveModule', 'jomi.feedbackModule', 'jomi.changePasswordModule',
  'jomi.settingsModule',
  'jomi.http.invoke',

  'jomi.popupService'
])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('login', {
    url: '/',
    templateUrl: 'templates/login/login.html',
    controller: 'loginController'
  })
  .state('home', {
    url: '/home',
    templateUrl: 'templates/home/home.html',
    controller: 'homeController'
  })
  .state('aboutUs', {
    url: '/aboutUs',
    templateUrl: 'templates/aboutUs/aboutUs.html',
    controller: 'aboutUsController'
  })
  .state('myProjectList', {
    url: '/myProjectList',
    templateUrl: 'templates/project/myProjectList.html',
    controller: 'projectController'
  })
  .state('myTaskList', {
    url: '/myTaskList',
    templateUrl: 'templates/task/myTaskList.html',
    controller: 'taskController'
  })
  .state('taskDetail', {
    url: '/taskDetail',
    templateUrl: 'templates/task/taskDetail.html',
    controller: 'taskDetailController',
    params:{parameter:{}}
  })
  .state('leaveForm', {
    url: '/leaveForm',
    templateUrl: 'templates/leave/leavePage.html',
    controller: 'leaveController'
  })
  .state('feedbackForm', {
    url: '/feedbackForm',
    templateUrl: 'templates/feedback/feedbackPage.html',
    controller: 'feedbackController'
  })
  .state('settingList', {
    url: '/settingList',
    templateUrl: 'templates/settings/settingPage.html',
    controller: 'settingsController'
  })
  .state('changePasswordForm', {
    url: '/changePasswordForm',
    templateUrl: 'templates/settings/changePassPage.html',
    controller: 'changePasswordController'
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');
});
